
 export default {
     namespaced : true,
     state : {
         kondisi : false,
         component : '',
         params : {}
     },
     mutations : {
         setKondisi : (state, kondisi) => {
             state.kondisi = kondisi
         },
         setComponent : (state, {component, params})=>{
             state.component = component
             state.params = params
         },
     },
     actions : {
         setKondisi : ({commit}, kondisi)=>{
             commit('setKondisi', kondisi)
         },
         setComponent : ({commit}, {component, params}) => {
             commit('setComponent', {component, params})
             commit('setKondisi', true)
         },
     },
     getters : {
         kondisi : state => state.kondisi,
         component : state => state.component,
         params : state => state.params
     }
 }